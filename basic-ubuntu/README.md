## basic-ubuntu agent - OS/Tool versions

| OS / Tool | Version |
| -----------------|---------|
| OS release <br> (cat /etc/issue) | Ubuntu 22.04.2 LTS \n \l |
| Java | openjdk version "11.0.17" 2022-10-18<br>IBM Semeru Runtime Open Edition 11.0.17.0 (build 11.0.17+8)<br>Eclipse OpenJ9 VM 11.0.17.0 (build openj9-0.35.0, JRE 11 Linux amd64-64-Bit Compressed References 20221031_559 (JIT enabled, AOT enabled)<br>OpenJ9   - e04a7f6c1<br>OMR      - 85a21674f<br>JCL      - a94c231303 based on jdk-11.0.17+8) |
| Git | git version 2.39.2 |
| SSH | OpenSSH_8.9p1 Ubuntu-3ubuntu0.1, OpenSSL 3.0.2 15 Mar 2022 |
| bash | GNU bash, version 5.1.16(1)-release (x86_64-pc-linux-gnu) |
| Wget | GNU Wget 1.21.2 built on linux-gnu. |
| cURL | curl 7.81.0 (x86_64-pc-linux-gnu) libcurl/7.81.0 OpenSSL/3.0.2 zlib/1.2.11 brotli/1.0.9 zstd/1.4.8 libidn2/2.3.2 libpsl/0.21.0 (+libidn2/2.3.2) libssh/0.9.6/openssl/zlib nghttp2/1.43.0 librtmp/2.3 OpenLDAP/2.5.13 |
| GPG | gpg (GnuPG) 2.2.27 |