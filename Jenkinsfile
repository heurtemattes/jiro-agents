@Library('releng-pipeline@main') _

pipeline {
  agent {
    kubernetes {
      yaml loadOverridableResource(
        libraryResource: 'org/eclipsefdn/container/agent.yml'
      )
    }
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '5'))
    timeout(time: 90, unit: 'MINUTES')
  }

  environment {
    HOME = '${env.WORKSPACE}'
    CREDENTIALS_ID = 'docker-bot-token'
    BUILD_DIR = './target'
  }

  triggers {
    cron('H H * * H')
  }

  stages {
    stage('Build Agents') {
      steps {
        container('containertools') {
          sh '''
            mkdir -p "${BUILD_DIR}"
            jsonnet agents.jsonnet > "${BUILD_DIR}/agents.json"
          '''
        }
      }
    }
    stage('Build Images') {
      steps {
        script {
          def agentIds = sh(script: "jq -r '. | keys[]' ${env.BUILD_DIR}/agents.json", returnStdout: true).trim().split('\n')
          agentIds.each { id ->
              buildAgent(id)
          }
        }
      }
    }
  }
}

def buildAgent(id) {
    String configDir = "${env.BUILD_DIR}/${id}"
    String config = "${configDir}/agent.json"
    String AGENTS_JSON="${env.BUILD_DIR}/agents.json"

    println "Building jiro-agent spec '${id}'"

    sh "mkdir -p ${configDir}"
    sh "jq -r '.[\"${id}\"]' ${AGENTS_JSON} > ${config}"
    sh "jq -r '.spec.docker.dockerfile' ${config} > ${configDir}/Dockerfile"

    String name = sh(script: "jq -r '.spec.docker.registry' ${config}", returnStdout: true).trim() +
                "/" + sh(script: "jq -r '.spec.docker.repository' ${config}", returnStdout: true).trim() +
                "/" + sh(script: "jq -r '.spec.docker.image' ${config}", returnStdout: true).trim()
    String version = sh(script: "jq -r '.spec.docker.tag' ${config}", returnStdout: true).trim()
    String context = sh(script: "jq -r '.spec.docker.context' ${config}", returnStdout: true).trim()

    buildImage(name, version, "", "${configDir}/Dockerfile", context)

    sh(script: "jq -r '.variants | keys[]' ${config}", returnStdout: true).eachLine { variant ->
        buildAgentVariant(id, variant, config)
    }
}

def buildAgentVariant(id, variant, agentConfig) {
    def configDir = "${env.BUILD_DIR}/${id}/${variant}"
    def config = "${configDir}/variant.json"

    println "Building jiro-agent '${id}' - variant ${variant}"

    sh "mkdir -p ${configDir}"
    sh "jq -r '.variants[\"${variant}\"]' ${agentConfig} > ${config}"
    sh "jq -r '.docker.dockerfile' ${config} > ${configDir}/Dockerfile"

    String name = sh(script: "jq -r '.spec.docker.registry' ${agentConfig}", returnStdout: true).trim() +
                "/" + sh(script: "jq -r '.spec.docker.repository' ${agentConfig}", returnStdout: true).trim() +
                "/" + sh(script: "jq -r '.spec.docker.image' ${agentConfig}", returnStdout: true).trim()
    String version = sh(script: "jq -r '.docker.tag' ${config}", returnStdout: true).trim()

    String aliases = sh(script: "jq -r '.docker.aliases | join(\",\")' ${config}")
    buildImage(name, version, aliases, "${configDir}/Dockerfile", "${configDir}")
}

def buildImage(String name, String version, String aliases = "", String dockerfile, String context, Map<String, String> buildArgs = [:], boolean latest = false) {
  String distroName = name + ':' + version
  println '############ buildImage ' + distroName + ' ############'
  def containerBuildArgs = buildArgs.collect { k, v -> '--opt build-arg:' + k + '=' + v }.join(' ')

  container('containertools') {
    containerBuild(
      credentialsId: env.CREDENTIALS_ID,
      name: name,
      version: version,
      aliases: aliases,
      dockerfile: dockerfile,
      context: context,
      buildArgs: containerBuildArgs,
      //push: env.GIT_BRANCH == 'master',
      push: false,
      latest: latest,
      debug: true
    )
  }
}
